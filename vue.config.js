const CompressionWebpackPlugin = require('compression-webpack-plugin');
const productionGzipExtensions = ['js', 'css'];
const isProduction = process.env.NODE_ENV === 'production';

module.exports = {
  /** 区分打包环境与开发环境
     * process.env.NODE_ENV==='production'  (打包环境)
     * process.env.NODE_ENV==='development' (开发环境)
     * baseUrl: process.env.NODE_ENV==='production'?"https://cdn.didabisai.com/front/":'front/',
     */

  // 基本路径
  baseUrl: process.env.NODE_ENV === 'production' ? '/web/' : '/',

  // 输出文件目录
  outputDir: 'dist',
  assetsDir:'admin',
  productionSourceMap:false,

  // 端口
  devServer: {
    host: '192.168.10.188',
    port: 9099,

    // 设置代理
    proxy: {
      "/api": {
        //target: "http://lear.ambcon.cn/meeting", // 域名
        //target: "http://192.168.10.122:8082/meeting", // 域名
         target: "http://192.168.10.188:8082/meeting",
         //target: "http://meeting.leardfm.com/meeting",
        ws: false, // 是否启用websockets
        changOrigin: true, //开启代理：在本地会创建一个虚拟服务端，然后发送请求的数据，并同时接收请求的数据，这样服务端和服务端进行数据的交互就不会有跨域问题
        pathRewrite:{
          '/api':''
        }
      }
    }
  },
  configureWebpack: config => {
    if (isProduction) {
      config.plugins.push(new CompressionWebpackPlugin({
        algorithm: 'gzip',
        test: new RegExp('\\.(' + productionGzipExtensions.join('|') + ')$'),
        threshold: 10240,
        minRatio: 0.8
      }));

      config.externals = {
        'vue': 'Vue',
        'vuex': 'Vuex',
      }
    }
  }
};