window.socketPath=window.location.href.indexOf('192.168.10')>-1?'ws://192.168.10.122:8082/meeting':'ws://localhost:8082/meeting'
window.reConnect_qa = function () {
  var socket_qa = new WebSocket(socketPath + '/passWebsocket/14/0')
  socket_qa.onopen = function () {
    console.log('open qa')
  }
  socket_qa.onmessage = function (res) {
    if(res.data!=="ping")_bus.$emit('showNextQA', res.data)
  }
  socket_qa.onclose = function () {
    console.log('close')
  }
  socket_qa.onerror = function () {
    console.log('error')
  }
  window.socket_qa = socket_qa
  setInterval(()=>{
    socket_qa.send("ping")
  },10000)
}
reConnect_qa()


window.reConnect_barrage = function () {
  var socket_barrage = new WebSocket(socketPath + '/websocket')
  socket_barrage.onopen = function () {
    console.log('open barrage')
  }
  socket_barrage.onmessage = function (res) {
    console.log('开启弹幕')
    _bus.$emit('send_barrage', res.data)
  }
  socket_barrage.onclose = function () {
    console.log('close')
  }
  socket_barrage.onerror = function () {
    console.log('error')
  }
  window.socket_barrage = socket_barrage
}
reConnect_barrage()

window.reConnect_topic = function () {
  var socket_topic = new WebSocket(socketPath + '/topicWebsocket/0/3')
  socket_topic.onopen = function () {
    console.log('开启议题')
  }
  socket_topic.onmessage = function (res) {
    console.log('发送议题人')
  }
  socket_topic.onclose = function () {
    console.log('close')
  }
  socket_topic.onerror = function () {
    console.log('error')
  }
  window.socket_topic = socket_topic
  setInterval(()=>{
    socket_topic.send("ping")
  },10000)
}

reConnect_topic()