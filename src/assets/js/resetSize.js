window.onresize = function () {
  var height = document.querySelector(".work-menu").clientHeight
  $(".bm-content").css({
    height: `calc(100% - ${height}px)`
  })
}