import Vue from 'vue'
import { 
  Button,
  Switch,
  Input,
  Message,
  Table,
  TableColumn,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Pagination,
  Dialog,
  Select,
  Option,
  Upload,
  Tabs,
  TabPane,
  DatePicker,
  Progress,
} from 'element-ui'
const pluginsList=[
  Button,
  Switch,
  Input,
  Table,
  TableColumn,
  Menu,
  MenuItem,
  MenuItemGroup,
  Submenu,
  Pagination,
  Dialog,
  Select,
  Option,
  Upload,
  Tabs,
  TabPane,
  DatePicker,
  Progress,
]
pluginsList.forEach(ele=>{
  Vue.use(ele)
})

Vue.prototype.$message = Message;