import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    test:0,
    // 确认弹框
    confirmInfo:{
      show:false,
      message:"",
      id:'',
      OK:function(){
        console.log("confirmInfo")
      },
      Cancel:function(){
        console.log("cancel")
      }
    },
    alertInfo:{
      show:false,
      message:"",
      OK:function(){
        console.log("confirmInfo")
      }
    },
    
    // 添加或者编辑
    add:{
      info:{
        id:"",//有则为编辑，无未新增
        userData:[],
        topicIds:[],//议题
        depIds:[],
        seatPic:'',
        headPic:'',
        material:'',
        fileName:'',
        materialUrl:'',//附件地址--->最终换成字符串
      },
      show:false,
      OK:function(){
        console.log("add--edit")
      }
    },
    
    // 会议回写记录
    records:{
      depVote:[],
      meetingInfo:{},//会议基本信息
      meetingPerson:[],//参会人员
      meetingTopic:[],//议题
    },

    delete:{
      ids:[],
      idroles:[],
    },

    // 下拉数据集合
    selectData:{
      rolesList:{//角色
        data:[
          {label:"领导",value:"0"},
          {label:"组长",value:"1"},
          {label:"员工",value:"2"},
        ],
      },
      leadersList:{//领导
        data:[
          {label:"test",value:"1"}
        ],
        settime:{
          url:'topicPoint/selectLeaderList.do',
          time:3600,//s 默认1小时刷新
          settime:function(){console.log("定时刷新")}
        },
      },
      headersList:{//组长
        data:[
          {label:"test",value:"1"}
        ],
        settime:{
          url:'',
          time:3600,//s 默认1小时刷新
          settime:function(){console.log("定时刷新")}
        },
      },
      departsList:{//部门
        data:[
          {label:"test",value:"1"}
        ],
        settime:{
          url:'',
          time:3600,//s 默认1小时刷新
          settime:function(){console.log("定时刷新")}
        },
      }
    }
  },
  mutations: {

  },
  actions: {

  }
})
