// 所有添加和编辑数据集合
const addData=[
  {
    path:'/manageHome',
    data3:[
      {key:'switch',type:'switch',label:'显示'},
      {key:'name',type:'input',label:'名称'},
      {key:'ename',type:'input',label:'英文名'},
      {key:'picture',type:'upload',label:'图片'},
    ],
    data4:[
      {key:'switch',type:'switch',label:'显示'},
      {key:'name',type:'input',label:'名称'},
      {key:'ename',type:'input',label:'英文名'},
      {key:'picture',type:'upload',label:'图片'},
      {key:'content',type:'upload',label:'背景'},
    ]
  },
  {
    path:'/manageIssues',//当前路由
    data:[
      {key:'name',type:'textarea',label:'议题'}
    ]
  },{
    path:'/manageIssuesGrages',
    data:[
      {key:'topicName',type:'label',label:'议题'},
      {key:'userId',type:'select',label:'姓名',array:'headersList'/* 组长列表 */,}
    ]
  },{
    path:'/manageDeparts',
    data:[
      {key:'name',type:'input',label:'部门'},
      {key:'picture',type:'upload',label:'头像'},
    ]
  },{
    path:'/manageRoles/leaders',
    data:[
      {key:'name',type:'input',label:'姓名'},
      {key:'job',type:'select',label:'角色',array:'rolesList'/* 角色列表 */,},
      {key:'depName',type:'select',label:'部门',array:'departsList'/* 部门列表 */,},
      {key:'phone',type:'input',label:'电话'},
      {key:'seatNum',type:'input',label:'桌号'},
      {key:'headPic',type:'upload',label:'头像'},
    ]
  },{
    path:'/manageRoles/headers',
    data:[
      {key:'name',type:'input',label:'姓名'},
      {key:'job',type:'select',label:'角色',array:'rolesList'/* 角色列表 */,},
      {key:'depName',type:'select',label:'部门',array:'departsList'/* 部门列表 */,},
      {key:'phone',type:'input',label:'电话'},
      {key:'seatNum',type:'input',label:'桌号'},
      {key:'headPic',type:'upload',label:'头像'},
    ]
  },{
    path:'/manageRoles/members',
    data:[
      {key:'name',type:'input',label:'姓名'},
      {key:'job',type:'select',label:'角色',array:'rolesList'/* 角色列表 */,},
      {key:'depName',type:'select',label:'部门',array:'departsList'/* 部门列表 */,},
      {key:'phone',type:'input',label:'电话'},
      {key:'seatNum',type:'input',label:'桌号'},
      {key:'headPic',type:'upload',label:'头像'},
    ]
  },{
    path:'/questions/manages',
    data:[
      {key:'content',type:'input',label:'题目'},
      {key:'optionA',type:'input',label:'选项A'},
      {key:'optionB',type:'input',label:'选项B'},
      {key:'optionC',type:'input',label:'选项C'},
      {key:'optionD',type:'input',label:'选项D'},
      {key:'rightAnswer',type:'input',label:'正确'},
    ]
  }
]

export default addData