import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import qs from 'qs'

import './plugins/element.js'
import './assets/js/socket'

// 引入字体图标
import './assets/icons/iconfont.css'
import './assets/css/iconfont_st.css'
// 初始化
import './assets/css/bac.css'
import './assets/css/reset.css'
import './assets/css/common.css'
import './assets/css/fontsize.css'
import './assets/css/margin.css'
import './assets/css/filter.css'
import './assets/css/ele_st.css'
import './assets/css/media.css'
import './assets/css/scroller.css'

// 缩放
// import './assets/js/resetSize'

window.$axios=axios
window.qs=qs
window.PATH=window.location.href.indexOf('192.168.10')>-1?'/api/':'/meeting/'
//上传地址
window.picPath=PATH+'homePage/uploadPicture.do' 
//下载模板地址
window.downloadPath=(window.location.href.indexOf('192.168.10')>-1?'http://192.168.10.122:8082':'http://localhost:8082/meeting') + '/meeting/'

// 定义总线
window._bus=new Vue()
import _g from './assets/js/globalFunctions'
window._g=_g

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

router.afterEach((to,from)=>{
  if(sessionStorage.getItem('canLogin')!='true'){
    router.replace('/')
  }
})