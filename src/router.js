import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
export default new Router({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path:"/",
      component: () => import('./views/login/login.vue')
    },{
      path:"/model",
      component: () => import('./components/model/model.vue'),
      children:[
        {
          path:'/works',
          redirect:"/works/qa",//重定向
          component: () => import('./views/works/works.vue'),
          children:[
            {
              path:'/works/qa',
              component: () => import('./views/works/model/qa.vue'),
            },{
              path:'/works/notes',
              component: () => import('./views/works/model/notes.vue'),
            },{
              path:'/works/scores',
              component: () => import('./views/works/model/score.vue'),
            },{
              path:'/works/extract',
              component: () => import('./views/works/model/extract.vue'),
            },{
              path:'/works/barrage',
              component: () => import('./views/works/model/barrage.vue'),
            },
          ]
        },{
          path:'/manageHome',
          component: () => import('./views/manageHome/manageHome.vue'),
        },{
          path:'/manageIssues',
          component: () => import('./views/manageIssues/manageIssues.vue'),
        },{
          path:'/manageIssuesGrages',
          component: () => import('./views/manageIG/manageIG.vue'),
        },{
          path:'/manageDeparts',
          component: () => import('./views/manageDeparts/manageDeparts.vue'),
        },{
          path:'/manageBarrage',
          component: () => import('./views/manageBarrage/manageBarrage.vue'),
        },{
          path:'/manageRoles',
          redirect:"/manageRoles/leaders",
          component: () => import('./views/manageRoles/manageRoles.vue'),
          children:[
            {
              path:'/manageRoles/leaders',
              name:'leaders',
              component: () => import('./views/manageRoles/leaders.vue'),
            },{
              path:'/manageRoles/headers',
              name:'headers',
              component: () => import('./views/manageRoles/headers.vue'),
            },{
              path:'/manageRoles/members',
              name:'members',
              component: () => import('./views/manageRoles/members.vue'),
            }
          ]
        },{
          path:'/manageMeetings',
          redirect:'/manageMeetings/add',
          component: () => import('./views/manageMeetings/manageMeetings.vue'),
          children:[
            {
              path:'/manageMeetings/add',
              component: () => import('./views/manageMeetings/add/add.vue'),
            },{
              path:'/manageMeetings/records/edit/:id',
              component: () => import('./views/manageMeetings/add/add.vue'),
            },{
              path:'/manageMeetings/records/look/:id',
              component: () => import('./views/manageMeetings/detail/add.vue'),
            },{
              path:'/manageMeetings/records',
              component: () => import('./views/manageMeetings/records/records.vue'),
            }
          ]
        },{
          path:'/questions',
          redirect:'/questions/settings',
          component: resolve => require(['./views/qas/qas.vue'], resolve),
          children:[
            {
              path:'/questions/settings',
              component: resolve => require(['./views/qas/settings.vue'], resolve),
            },{
              path:'/questions/manages',
              component: resolve => require(['./views/qas/manages.vue'], resolve),
            }
          ]
        }
      ]
    }
  ]
})
